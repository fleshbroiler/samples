﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("starting");

            FileInfo inFle = new FileInfo(@"C:\dev\FlatSpotCal\data\2015-01-14 21-55-03 12141\FlatSpotA 2015-01-14 21-55-13.6512141-0001.fspt");

            List<string[]> fsData = new List<string[]>();
            char[] delimiter = { ',' };
            try
            {
                using (StreamReader sr = new StreamReader(inFle.ToString()))
                {
                    string line = sr.ReadLine();
                    while (line != null && line != String.Empty)
                    {
                        //Console.WriteLine(line);
                        
                        string[] data = line.Split(delimiter);
                        fsData.Add(data);
                        line = sr.ReadLine();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }


            Console.WriteLine("fin");
            Console.ReadLine();
        }



    }
}
