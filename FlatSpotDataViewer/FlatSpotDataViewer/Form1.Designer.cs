﻿namespace FlatSpotDataViewer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.browseBttn = new System.Windows.Forms.Button();
            this.dataPath = new System.Windows.Forms.TextBox();
            this.dataList = new System.Windows.Forms.ListBox();
            this.nextBttn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.fileLabel = new System.Windows.Forms.Label();
            this.view_single_record = new System.Windows.Forms.CheckBox();
            this.railLabel = new System.Windows.Forms.Label();
            this.sessionLabel = new System.Windows.Forms.Label();
            this.brwseBttnCal = new System.Windows.Forms.Button();
            this.outputPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.selectInputFileBttn = new System.Windows.Forms.Button();
            this.selectedInputData = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.countSelectedA = new System.Windows.Forms.TextBox();
            this.countSelectedB = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.axleLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(12, 264);
            this.chart1.Name = "chart1";
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series13.Legend = "Legend1";
            series13.Name = "Series1";
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series14.Legend = "Legend1";
            series14.Name = "Series2";
            series15.ChartArea = "ChartArea1";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series15.Legend = "Legend1";
            series15.Name = "Series3";
            series16.ChartArea = "ChartArea1";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series16.Legend = "Legend1";
            series16.Name = "Series4";
            series17.ChartArea = "ChartArea1";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series17.Legend = "Legend1";
            series17.Name = "Series5";
            series18.ChartArea = "ChartArea1";
            series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series18.Legend = "Legend1";
            series18.Name = "Series6";
            this.chart1.Series.Add(series13);
            this.chart1.Series.Add(series14);
            this.chart1.Series.Add(series15);
            this.chart1.Series.Add(series16);
            this.chart1.Series.Add(series17);
            this.chart1.Series.Add(series18);
            this.chart1.Size = new System.Drawing.Size(1170, 527);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // browseBttn
            // 
            this.browseBttn.Location = new System.Drawing.Point(12, 11);
            this.browseBttn.Name = "browseBttn";
            this.browseBttn.Size = new System.Drawing.Size(91, 23);
            this.browseBttn.TabIndex = 1;
            this.browseBttn.Text = "browse fspt";
            this.browseBttn.UseVisualStyleBackColor = true;
            this.browseBttn.Click += new System.EventHandler(this.browseBttn_Click);
            // 
            // dataPath
            // 
            this.dataPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataPath.Location = new System.Drawing.Point(110, 12);
            this.dataPath.Name = "dataPath";
            this.dataPath.Size = new System.Drawing.Size(370, 22);
            this.dataPath.TabIndex = 2;
            this.dataPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dataList
            // 
            this.dataList.FormattingEnabled = true;
            this.dataList.Location = new System.Drawing.Point(595, 29);
            this.dataList.Name = "dataList";
            this.dataList.Size = new System.Drawing.Size(299, 225);
            this.dataList.TabIndex = 3;
            this.dataList.SelectedIndexChanged += new System.EventHandler(this.dataList_SelectedIndexChanged);
            // 
            // nextBttn
            // 
            this.nextBttn.Location = new System.Drawing.Point(514, 78);
            this.nextBttn.Name = "nextBttn";
            this.nextBttn.Size = new System.Drawing.Size(75, 23);
            this.nextBttn.TabIndex = 4;
            this.nextBttn.Text = "next";
            this.nextBttn.UseVisualStyleBackColor = true;
            this.nextBttn.Click += new System.EventHandler(this.nextBttn_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(514, 107);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "previous";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // fileLabel
            // 
            this.fileLabel.AutoSize = true;
            this.fileLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileLabel.Location = new System.Drawing.Point(121, 236);
            this.fileLabel.Name = "fileLabel";
            this.fileLabel.Size = new System.Drawing.Size(47, 15);
            this.fileLabel.TabIndex = 6;
            this.fileLabel.Text = "label1";
            // 
            // view_single_record
            // 
            this.view_single_record.AutoSize = true;
            this.view_single_record.Checked = true;
            this.view_single_record.CheckState = System.Windows.Forms.CheckState.Checked;
            this.view_single_record.Location = new System.Drawing.Point(15, 74);
            this.view_single_record.Name = "view_single_record";
            this.view_single_record.Size = new System.Drawing.Size(81, 17);
            this.view_single_record.TabIndex = 7;
            this.view_single_record.Text = "View Single";
            this.view_single_record.UseVisualStyleBackColor = true;
            this.view_single_record.CheckedChanged += new System.EventHandler(this.view_single_record_CheckedChanged);
            // 
            // railLabel
            // 
            this.railLabel.AutoSize = true;
            this.railLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.railLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.railLabel.Location = new System.Drawing.Point(160, 173);
            this.railLabel.Name = "railLabel";
            this.railLabel.Size = new System.Drawing.Size(35, 31);
            this.railLabel.TabIndex = 8;
            this.railLabel.Text = "C";
            // 
            // sessionLabel
            // 
            this.sessionLabel.AutoSize = true;
            this.sessionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sessionLabel.ForeColor = System.Drawing.Color.Black;
            this.sessionLabel.Location = new System.Drawing.Point(184, 79);
            this.sessionLabel.Name = "sessionLabel";
            this.sessionLabel.Size = new System.Drawing.Size(51, 16);
            this.sessionLabel.TabIndex = 9;
            this.sessionLabel.Text = "label1";
            // 
            // brwseBttnCal
            // 
            this.brwseBttnCal.Location = new System.Drawing.Point(12, 40);
            this.brwseBttnCal.Name = "brwseBttnCal";
            this.brwseBttnCal.Size = new System.Drawing.Size(91, 23);
            this.brwseBttnCal.TabIndex = 10;
            this.brwseBttnCal.Text = "browse cal. dir. ";
            this.brwseBttnCal.UseVisualStyleBackColor = true;
            this.brwseBttnCal.Click += new System.EventHandler(this.brwseBttnCal_Click);
            // 
            // outputPath
            // 
            this.outputPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputPath.Location = new System.Drawing.Point(110, 41);
            this.outputPath.Name = "outputPath";
            this.outputPath.Size = new System.Drawing.Size(370, 22);
            this.outputPath.TabIndex = 11;
            this.outputPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(489, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 16);
            this.label1.TabIndex = 12;
            this.label1.Text = "fspt data";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(490, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 16);
            this.label2.TabIndex = 13;
            this.label2.Text = "output cal dir.";
            // 
            // selectInputFileBttn
            // 
            this.selectInputFileBttn.Location = new System.Drawing.Point(514, 148);
            this.selectInputFileBttn.Name = "selectInputFileBttn";
            this.selectInputFileBttn.Size = new System.Drawing.Size(75, 23);
            this.selectInputFileBttn.TabIndex = 14;
            this.selectInputFileBttn.Text = "select";
            this.selectInputFileBttn.UseVisualStyleBackColor = true;
            this.selectInputFileBttn.Click += new System.EventHandler(this.selectInputFileBttn_Click);
            // 
            // selectedInputData
            // 
            this.selectedInputData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.selectedInputData.FormattingEnabled = true;
            this.selectedInputData.Location = new System.Drawing.Point(900, 95);
            this.selectedInputData.Name = "selectedInputData";
            this.selectedInputData.Size = new System.Drawing.Size(282, 160);
            this.selectedInputData.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(120, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "session:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(125, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 15);
            this.label4.TabIndex = 17;
            this.label4.Text = "rail:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(591, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 15);
            this.label5.TabIndex = 18;
            this.label5.Text = "flat spot data:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(897, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 15);
            this.label6.TabIndex = 19;
            this.label6.Text = "selected cal data:";
            // 
            // countSelectedA
            // 
            this.countSelectedA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countSelectedA.ForeColor = System.Drawing.Color.Navy;
            this.countSelectedA.Location = new System.Drawing.Point(1114, 42);
            this.countSelectedA.Name = "countSelectedA";
            this.countSelectedA.Size = new System.Drawing.Size(68, 21);
            this.countSelectedA.TabIndex = 20;
            // 
            // countSelectedB
            // 
            this.countSelectedB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countSelectedB.ForeColor = System.Drawing.Color.Navy;
            this.countSelectedB.Location = new System.Drawing.Point(1114, 68);
            this.countSelectedB.Name = "countSelectedB";
            this.countSelectedB.Size = new System.Drawing.Size(68, 21);
            this.countSelectedB.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(976, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 15);
            this.label7.TabIndex = 22;
            this.label7.Text = "num of selected A files:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(976, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 15);
            this.label8.TabIndex = 23;
            this.label8.Text = "num of selected B files:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(121, 136);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 15);
            this.label9.TabIndex = 25;
            this.label9.Text = "axle:";
            // 
            // axleLabel
            // 
            this.axleLabel.AutoSize = true;
            this.axleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.axleLabel.ForeColor = System.Drawing.Color.Black;
            this.axleLabel.Location = new System.Drawing.Point(171, 136);
            this.axleLabel.Name = "axleLabel";
            this.axleLabel.Size = new System.Drawing.Size(19, 20);
            this.axleLabel.TabIndex = 24;
            this.axleLabel.Text = "1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1194, 803);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.axleLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.countSelectedB);
            this.Controls.Add(this.countSelectedA);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.selectedInputData);
            this.Controls.Add(this.selectInputFileBttn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.outputPath);
            this.Controls.Add(this.brwseBttnCal);
            this.Controls.Add(this.sessionLabel);
            this.Controls.Add(this.railLabel);
            this.Controls.Add(this.view_single_record);
            this.Controls.Add(this.fileLabel);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.nextBttn);
            this.Controls.Add(this.dataList);
            this.Controls.Add(this.dataPath);
            this.Controls.Add(this.browseBttn);
            this.Controls.Add(this.chart1);
            this.Name = "Form1";
            this.Text = "Flat Spot Data Viewer";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button browseBttn;
        private System.Windows.Forms.TextBox dataPath;
        private System.Windows.Forms.ListBox dataList;
        private System.Windows.Forms.Button nextBttn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label fileLabel;
        private System.Windows.Forms.CheckBox view_single_record;
        private System.Windows.Forms.Label railLabel;
        private System.Windows.Forms.Label sessionLabel;
        private System.Windows.Forms.Button brwseBttnCal;
        private System.Windows.Forms.TextBox outputPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button selectInputFileBttn;
        private System.Windows.Forms.ListBox selectedInputData;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox countSelectedA;
        private System.Windows.Forms.TextBox countSelectedB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label axleLabel;
    }
}

