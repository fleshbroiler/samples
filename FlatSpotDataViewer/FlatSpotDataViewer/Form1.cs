﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using System.Windows.Forms.DataVisualization;

namespace FlatSpotDataViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitChart();
        }



        private void InitChart()
        {
            //chart1.Series[0].Points.AddXY(1.0, 1.0);
            //chart1.Series[0].Points.AddXY(2.0, 2.0);
            //chart1.Series[0].Points.AddXY(3.0, 3.0);
            //chart1.Series[0].Points.AddXY(4.0, 4.0);
            //chart1.Series[0].Points.AddXY(5.0, 3.0);
            //chart1.Series[0].Points.AddXY(6.0, 2.0);
            //chart1.Series[0].Points.AddXY(7.0, 1.0);
        }

        private void browseBttn_Click(object sender, EventArgs e)
        {
            DialogResult dr = folderBrowserDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                dataPath.Text = folderBrowserDialog1.SelectedPath;
                handleDataPathSelection();
            }
        }

        private void handleDataPathSelection()
        {
            dataList.Items.Clear();
            DirectoryInfo di = new DirectoryInfo(folderBrowserDialog1.SelectedPath);
            FileInfo[] children = di.GetFiles("*.fspt");
            if (children != null && children.Length > 0)
            {
                dataList.Items.AddRange(children);
                ClearChart();
                dataList.SelectedIndex = 0;
            }
        }

        private void dataList_SelectedIndexChanged(object sender, EventArgs e)
        {
            fileLabel.Text = dataList.SelectedItem.ToString();
            setDataLabels();
            parseAndPlotSelected();
        }

        private void nextBttn_Click(object sender, EventArgs e)
        {
            if (dataList.SelectedIndex + 1 == dataList.Items.Count)
            {
                dataList.SelectedIndex = 0;
            }
            else
            {
                dataList.SelectedIndex = ++dataList.SelectedIndex;
            }
            fileLabel.Text = dataList.Items[dataList.SelectedIndex].ToString();
        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataList.SelectedIndex - 1 < 0)
            {
                dataList.SelectedIndex = (dataList.Items.Count - 1);
            }
            else
            {
                dataList.SelectedIndex = --dataList.SelectedIndex;
            }
            fileLabel.Text = dataList.Items[dataList.SelectedIndex].ToString();
        }
  
        private List<string[]> parseSelected()
        {
            List<string[]> fsData = new List<string[]>();
            char[] delimiter = { ',' };
            try
            {
               
                using (StreamReader sr = new StreamReader(@dataPath.Text+ @"\"+dataList.SelectedItem.ToString()))
                {
                    string line = sr.ReadLine();
                    while (line != null && line != String.Empty)
                    {
                        string[] data = line.Split(delimiter);
                        fsData.Add(data);
                        line = sr.ReadLine();
                    }
                }
            }
            catch (Exception e)
            {
//                Console.WriteLine("The file could not be read:");
//                Console.WriteLine(e.Message);
            }
            return fsData;
        }

        private void parseAndPlotSelected()
        {
            List<string[]> fsData = new List<string[]>();
            char[] delimiter = { ',' };
            try
            {
                using (StreamReader sr = new StreamReader(@dataPath.Text + @"\" + dataList.SelectedItem.ToString()))
                {
                    string line = sr.ReadLine();
                    while (line != null && line != String.Empty)
                    {
                        string[] data = line.Split(delimiter);
                        fsData.Add(data);
                        line = sr.ReadLine();
                    }
                }
            }
            catch (Exception e)
            {
                //                Console.WriteLine("The file could not be read:");
                //                Console.WriteLine(e.Message);
            }
            plotSelected(fsData);
        }

        private void ClearChart()
        {
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            chart1.Series[2].Points.Clear();
            chart1.Series[3].Points.Clear();
            chart1.Series[4].Points.Clear();
            chart1.Series[5].Points.Clear();
        }

        private void plotSelected(List<string[]> _fsData)
        {
            if (view_single_record.Checked)
            {
                ClearChart();
            }
            int listIndex = 1;
            int val = 0;
            foreach (string[] data in _fsData)
            {
                for (int arrayIndex = 0; arrayIndex < 6; arrayIndex++)
                {
                    string value = data[arrayIndex];
                    if (value != string.Empty)
                    {
                        val = int.Parse(value);
                        if (val > 0)
                        {
                            chart1.Series[arrayIndex].Points.AddXY(listIndex, val);
                        }                    
                    }                    
                }
                listIndex++;
            }
        }

        private void view_single_record_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void setDataLabels()
        {
            if (dataList.SelectedItem.ToString().Contains("FlatSpotA"))
            {
                railLabel.Text = "A";
            }
            else
            {
                railLabel.Text = "B";
            }

            int length = dataList.SelectedItem.ToString().LastIndexOf("-") 
                - (dataList.SelectedItem.ToString().IndexOf(".")) -3   ;

            sessionLabel.Text = dataList.SelectedItem.ToString().Substring(
                dataList.SelectedItem.ToString().IndexOf(".")+3, length);

            axleLabel.Text = dataList.SelectedItem.ToString().Substring(
                dataList.SelectedItem.ToString().LastIndexOf("-")+1, 4);
        }

        private void brwseBttnCal_Click(object sender, EventArgs e)
        {
            DialogResult dr = folderBrowserDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                outputPath.Text = folderBrowserDialog1.SelectedPath;
            }
            UpdateSelectedFiles();
        }

        private void selectInputFileBttn_Click(object sender, EventArgs e)
        {
            // copy selected file to dataPath
            if (outputPath.Text.Trim().Length > 3)
            {
                DirectoryInfo dnfo = new DirectoryInfo(outputPath.Text);
                if (dnfo.Exists)
                {
                    FileInfo fnfo = new FileInfo(dataPath.Text + @"\" + dataList.SelectedItem.ToString());
                    fnfo.CopyTo(dnfo.FullName+ @"\" + fnfo.Name);
                    UpdateSelectedFiles();
                }
                else
                {
                    MessageBox.Show("You must specify an output calibration directory", "output directory not specified" );
                }
                dnfo = null;
            }
            
        }

        private void UpdateSelectedFiles()
        {
            if (outputPath.Text.Trim().Length > 3)
            {
                DirectoryInfo dnfo = new DirectoryInfo(outputPath.Text);
                if (dnfo.Exists)
                {
                    selectedInputData.Items.Clear();
                    FileInfo[] children = dnfo.GetFiles("*.fspt");
                    selectedInputData.Items.AddRange(children);
                    UpdateSelectedCounts(children);
                }
                dnfo = null;
            }
        }

        private void UpdateSelectedCounts(FileInfo[] children)
        {
            List<FileInfo> files = new List<FileInfo>();
            files.AddRange(children);

            var result =
                    (from c in files.AsQueryable<FileInfo>()
                     where c.Name.Contains("FlatSpotA") select c).Count();
            countSelectedA.Text = result.ToString();

            result =
                    (from c in files.AsQueryable<FileInfo>()
                     where c.Name.Contains("FlatSpotB")
                     select c).Count();
            countSelectedB.Text = result.ToString();
            
        }
    }
}
